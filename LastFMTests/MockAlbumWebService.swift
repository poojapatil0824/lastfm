//
//  MockAlbumWebService.swift
//  LastFMTests
//
//  Created by Pooja Awati on 24/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import XCTest
@testable import LastFM

class  MockAlbumWebService {
    var shouldServiceReturnError = false
    var albumServiceRequestWasCalled = false
    var mockJSONData: [Album]?

    enum MockAlbumServiceError: Error{
        case album
    }
    func reset(){
        shouldServiceReturnError = false
        albumServiceRequestWasCalled = false
    }
    convenience init(){
        self.init(false)
    }
    init(_ shouldServiceReturnError: Bool) {
        self.shouldServiceReturnError = shouldServiceReturnError
        self.getMockJSONData()
    }

    func getMockJSONData(){
        if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: [])
                let decoder = JSONDecoder()
                let response = try decoder.decode(AlbumResponse.self, from: data)
                self.mockJSONData = response.results.albummatches.album

            } catch {
                print("Error loading JSON file")
            }
        } else {
            print("File not found")
        }
    }
}
extension MockAlbumWebService: AlbumWebServiceProtocol {
    func fetchAlbumsFromServer(completion: @escaping ([Album]?, Error?) -> Void) {
         albumServiceRequestWasCalled = true

               if shouldServiceReturnError {
                completion(nil, MockAlbumServiceError.album)
               } else {
                completion(self.mockJSONData, nil)
               }
    }

}

