//
//  AlbumWebServiceTests.swift
//  LastFMTests
//
//  Created by Pooja Awati on 24/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import XCTest
@testable import LastFM

class AlbumWebServiceTests: XCTestCase {

    let mockAlbumWebService = MockAlbumWebService()

    func testalbumWebServiceResponse() {
        let expectation = self.expectation(description: "Album Web Service Parse Expected")
        mockAlbumWebService.fetchAlbumsFromServer(){ json, error in

            XCTAssertNil(error)
            guard json != nil else {
                XCTFail()
                return
            }
            do {
                XCTAssertNotNil(json)
                expectation.fulfill()
            }
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }

}
