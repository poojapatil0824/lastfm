//
//  AlbumListTableViewCell.swift
//  LastFM
//
//  Created by Pooja Awati on 24/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class AlbumListTableViewCell: UITableViewCell {
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var albumImageView: UIImageView!

    func set(album: Album) {
        let imageDetails = album.image.filter { $0.size == "extralarge" }
        let imageUrl = imageDetails[0].text

        ImageCacheService.getImage(withURL: imageUrl) { image in
               self.albumImageView.image = image
           }
       }

}
