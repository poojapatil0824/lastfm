//
//  AlbumWebService.swift
//  LastFM
//
//  Created by Pooja Awati on 23/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class AlbumWebService {
    func fetchAlbumsFromServer (searchText: String, completion: @escaping (_ album : [Album]?, _ error: Error?) -> Void) {
        let API_KEY = "ca89c6265a401c9ef65ffe1d74b291d3"
        let url = URL(string: "http://ws.audioscrobbler.com/2.0/?method=album.search&album=\(searchText)&api_key=\(API_KEY)&format=json")
        guard let downloadURL = url else {return}
        let dataTask = URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let jsonData = data, error == nil, urlResponse != nil else {
                print("\(error.debugDescription)")
                return
            }
            do{
                let decoder = JSONDecoder()
                let albumsResponse = try decoder.decode(AlbumResponse.self, from: jsonData)
                let albumDetails = albumsResponse.results.albummatches.album
                return completion(albumDetails, nil)
            }catch {
                // Error handling can be done here
                print("error", error)
            }
        }
        dataTask.resume()
    }
}
