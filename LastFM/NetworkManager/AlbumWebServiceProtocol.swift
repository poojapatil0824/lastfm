//
//  AlbumWebServiceProtocol.swift
//  LastFM
//
//  Created by Pooja Awati on 24/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

protocol AlbumWebServiceProtocol {
    func fetchAlbumsFromServer(completion: @escaping (_ album : [Album]?, _ error: Error?) -> Void)
}

