//
//  AlbumDetailViewController.swift
//  LastFM
//
//  Created by Pooja Awati on 23/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class AlbumDetailViewController: UIViewController {
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    
    var albumDetails: Album?
    var albumDetailedImage = UIImage()

    override func viewDidLoad() {
    super.viewDidLoad()
        albumNameLabel.text = albumDetails?.name
        artistNameLabel.text = albumDetails?.artist
        detailImageView.image = albumDetailedImage
    }

    @IBAction func visitSiteButton(_ sender: Any) {
        let urlString = albumDetails?.url
        guard let url = URL(string: urlString!) else { return }
        UIApplication.shared.open(url)

    }
}
