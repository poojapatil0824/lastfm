//
//  AlbumListViewController.swift
//  LastFM
//
//  Created by Pooja Awati on 23/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import UIKit

class AlbumListViewController: UIViewController {
    @IBOutlet weak var albumListTableView: UITableView!

    @IBOutlet weak var albumSearchBar: UISearchBar!

    private var albumListArray = [Album]()
    var viewModel = AlbumViewModel(albumWebService: AlbumWebService())

    override func viewDidLoad() {
        super.viewDidLoad()
        albumSearchBar.delegate = self
        albumListTableView.tableFooterView = UIView(frame: .zero)
    }
    private func loadAlbumList(searchText: String) {
        viewModel.getFruitDataFromService(searchText: searchText) { (album, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                return
            }
            guard let album = album  else { return }
            self.albumListArray = album
            DispatchQueue.main.async {
                self.albumListTableView.reloadData()
            }
        }
    }
}

extension AlbumListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  albumListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCell") as! AlbumListTableViewCell
        cell.albumNameLabel?.text =  albumListArray[indexPath.row].name
        cell.set(album: albumListArray[indexPath.row])

        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let currentCell = tableView.cellForRow(at: indexPath) as! AlbumListTableViewCell
           let albumDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailVC") as! AlbumDetailViewController
           albumDetailViewController.albumDetailedImage = currentCell.albumImageView.image!
           albumDetailViewController.albumDetails = albumListArray[indexPath.row]
           self.navigationController?.pushViewController(albumDetailViewController, animated: true)
       }
}

extension AlbumListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchBarText = searchBar.text else { return }
        if searchBarText.count >= 2 {
            loadAlbumList(searchText:searchBarText)
        }
        else {
            self.albumListArray = []
            DispatchQueue.main.async {
                self.albumListTableView.reloadData()
            }
        }
    }
}



