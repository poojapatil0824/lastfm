//
//  AlbumViewModel.swift
//  LastFM
//
//  Created by Pooja Awati on 23/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class AlbumViewModel {

    private var albumWebService: AlbumWebService

    init(albumWebService: AlbumWebService) {
        self.albumWebService = albumWebService
    }

    public func getFruitDataFromService(searchText:String, completion: @escaping (_ album : [Album]?, _ error: Error?) -> Void) {
        albumWebService.fetchAlbumsFromServer(searchText: searchText) { (album, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                return
            }
            guard let album = album  else { return }
            return completion(album, nil)
        }
    }
}



