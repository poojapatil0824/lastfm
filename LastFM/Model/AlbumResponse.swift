//
//  AlbumResponse.swift
//  LastFM
//
//  Created by Pooja Awati on 23/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

struct AlbumResponse: Codable {
    let results: Results
}

struct Results: Codable {
    let albummatches: Albummatches
}

struct Albummatches: Codable {
    let album: [Album]
}

struct Album: Codable {
    let name, artist: String
    let url: String
    let image: [Image]
}

struct Image: Codable {
    let text: URL
    let size: String
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case size
    }
}



