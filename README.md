##  LastFW App using MVVM design pattern

-   I have used **Swift 5** and **Xcode 11** to develop this app
-   This is **Mobile** app and deployment target is supported for **iOS 13**
-   I have implemented **MVVM** design pattern for the project architecture 
-   The Directory app has two screens,
    1.    Album List
    2.    Album Details
-   User can search for the Album in the search bar
-   I have assumed that minimum two characters should be entered to get the search results. 
-   I have also implemeneted **Image Caching** to load the images faster next time 
-	Unit tests for Album Webservice are implemented
-   I have also attached screenshots and application architecture diagram
